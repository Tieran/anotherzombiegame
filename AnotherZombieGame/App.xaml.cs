﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace AnotherZombieGame
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static Human _zombie;
        public static List<Human> _human = new List<Human>();
        private void Application_Exit(object sender, ExitEventArgs e)
        {
            StorageData.writeXML<List<Human>>(_human, "gameData.xml");
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            _human = StorageData.readXML<List<Human>>("gameData.xml");
        }
    }
}
