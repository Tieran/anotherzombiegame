﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AnotherZombieGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        Random rnd = new Random();
          
        public MainWindow()
        {
            InitializeComponent();
            timer.Interval = TimeSpan.FromSeconds(0.1);
            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
        }

        private void Canvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var curPosX = Canvas.GetLeft(el_demo);
            var curPosY = Canvas.GetTop(el_demo);

            Point toPos = e.GetPosition(game);

            double angl = Math.Tan((toPos.Y - curPosY) / (toPos.X - curPosX));

            DoubleAnimation daX = new DoubleAnimation();
            daX.To = toPos.X-15;
            DoubleAnimation daY = new DoubleAnimation();
            daY.To = toPos.Y-15;

            daX.Duration = daY.Duration = TimeSpan.FromSeconds(1);
            el_demo.BeginAnimation(Canvas.LeftProperty, daX);
            el_demo.BeginAnimation(Canvas.TopProperty, daY);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            side_grid.DataContext = App._zombie = new Human("Zombie", game);

            if(App._human == null)
            {
                App._human = new List<Human>();

                var hans = new Human("Hans", game);
                var peter = new Human("Peter", game);
                App._human.Add(hans);
                App._human.Add(peter);
            }
            else
            {
                oldState();
            }
        }

        private void oldState()
        {
            foreach(var item in App._human)
            {
                item.restore(game);
            }
        }       

        private void b_reset_Click(object sender, RoutedEventArgs e)
        {
            App._zombie.timer.Stop();
            game.Children.Remove(App._zombie.body);

            foreach (var item in App._human)
            {
                item.timer.Stop();
                game.Children.Remove(item.body);
            }

            initGame();
        }

        private void initGame()
        {
            side_grid.DataContext = App._zombie = new Human("Zombie", game);

            App._human = new List<Human>();

            var hans = new Human("Hans", game);
            var peter = new Human("Peter", game);
            App._human.Add(hans);
            App._human.Add(peter);
        }
    }
}
