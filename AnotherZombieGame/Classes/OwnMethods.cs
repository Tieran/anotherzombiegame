﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnotherZombieGame
{
    public class OwnMethods
    {
        public static double distance(double x1, double y1, double x2, double y2)
        {
            double result = Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            return result;
        }
    }
}
