﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AnotherZombieGame
{
    class StorageData
    {
        public static void writeBinary<T>(string datei, T zuSpeichern)
        {
            FileStream fs = null;
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                fs = new FileStream(datei, FileMode.Create);
                bf.Serialize(fs, zuSpeichern);
                fs.Close();
            }
            catch (Exception)
            {
                if(fs!=null)
                    fs.Close();
            }
        }
        
        internal static T readBinary<T>(string datei)
        {
            T output;
            BinaryFormatter bf = new BinaryFormatter();

            try
            {
                FileStream fs = new FileStream(datei, FileMode.OpenOrCreate);
                output = (T)bf.Deserialize(fs);
                fs.Close();
            }
            catch (Exception)
            {
                    
                output = default(T);
            }
            return output;
        }

        public static void writeXML<T>(T data, string dataname)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            FileStream stream;
            stream = new FileStream(dataname, FileMode.Create);
            serializer.Serialize(stream, data);
            stream.Close();
        }
        
        public static T readXML<T>(string dataname)
        {
            try
            {
                using(StreamReader sr = new StreamReader(dataname))
                {
                    XmlSerializer xmlSer = new XmlSerializer(typeof(T));
                    return (T)xmlSer.Deserialize(sr);
                }
            }
            catch (Exception)
            {
                return default(T);
            }
        }   
    }
}
