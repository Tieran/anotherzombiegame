﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace AnotherZombieGame
{
    public abstract class Character : INotifyPropertyChanged
    {
        [XmlIgnore]
        public Ellipse body;

        public int width = 30;
        public int height = 30;
        public string name { get; set; }
        public CharacterTyp cTyp;

        public static Random rnd = new Random();
        private double c_yPos, c_xPos, c_angle;

        public double yPos
        {
            get { return c_yPos; }
            set
            {
                c_yPos = value;
                OnPropertyChanged("yPos");
            }
        }
        
        public double xPos
        {
            get { return c_xPos; }
            set
            {
                c_xPos = value;
                OnPropertyChanged("xPos");
            }
        }

        public double angle
        {
            get { return c_angle; }
            set
            {
                c_angle = value;
                OnPropertyChanged("angle");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        
        private Canvas game;

        public Character() { }

        public Character(string cName, Canvas game)
        {
            this.game = game;

            createBody(game);
            xPos = rnd.Next(0, (int)(body.Parent as Canvas).ActualWidth);
            yPos = rnd.Next(0, (int)(body.Parent as Canvas).ActualHeight);
            angle = rnd.Next(0, 360);
            setBinding();

            name = cName;
            if (name == "Zombie")
            {
                body.Fill = Brushes.Green;
            }
        }

        private void setBinding()
        {
            Binding xBinding = new Binding("xPos");
            xBinding.Source = this;
            body.SetBinding(Canvas.LeftProperty, xBinding);

            Binding yBinding = new Binding("yPos");
            yBinding.Source = this;
            body.SetBinding(Canvas.TopProperty, yBinding);
        }

        public void moveCharacter()
        {
            if (xPos <= 0)
            {
                xPos = rnd.Next(5);
                angle = rnd.Next(0, 180);          
            }
            if(xPos>=(body.Parent as Canvas).ActualWidth - body.Width)
            {
                xPos = (body.Parent as Canvas).ActualWidth - body.Width;
                angle = rnd.Next(180, 360);
            }
            if (yPos <= 0)
            {
                yPos = rnd.Next(5);
                if (rnd.Next(0, 2) > 0)
                {
                    angle = rnd.Next(0, 90);
                }
                else
                {
                    angle = rnd.Next(270, 360);
                }
                
            }
            if(yPos>=(body.Parent as Canvas).ActualHeight - body.Height)
            {
                yPos = (body.Parent as Canvas).ActualHeight - body.Height;
                angle = rnd.Next(90, 270);
            }

            double radian = (Math.PI / 180) * angle;
            Point point = new Point { X = Math.Sin(radian), Y = Math.Cos(radian) };
            xPos += point.X * 2;
            yPos += point.Y * 2;
        }

        private void checkAngle(int mn, int mx)
        {
            angle = (180 - 2 * angle);
            angle += rnd.Next(mn, mx);
            angle = angle % 360;
        }

        public enum CharacterTyp
        {
            Player, Human, Zombie
        }

        public void createBody(Canvas game)
        {
            body = new Ellipse();
            body.Width = width;
            body.Height = height;
            body.Fill = Brushes.Red;
            game.Children.Add(body);
            setBinding();
        }
    }
}
