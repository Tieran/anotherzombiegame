﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Xml.Serialization;
using System.Speech.Synthesis;

namespace AnotherZombieGame
{
    public class Human : Character
    {
        private bool greeten = false;

        [XmlIgnore]
        public DispatcherTimer timer = new DispatcherTimer();

        public Human() { }

        public Human(string name, Canvas game) : base(name, game)
        {
            cTyp = CharacterTyp.Human;

            createTimer();
        }

        private void createTimer()
        {
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(0.01);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            moveCharacter();
            zombieHit();
        }

        SpeechSynthesizer ss = new SpeechSynthesizer();

        private void zombieHit()
        {
            if (!(name == "Zombie"))
            {
                if ((int)OwnMethods.distance(xPos, yPos, App._zombie.xPos, App._zombie.yPos) <= 100)
                {
                    if(ss.State == SynthesizerState.Ready)
                    {
                        ss.SpeakAsync("Hilfe ein Zombie");
                    }
                    ////angle = (App._zombie.angle + (90 - (App._zombie.angle - angle)));
                }
            }
        }

        internal void restore(Canvas game)
        {
            createBody(game);
            createTimer();
        }
    }
}
